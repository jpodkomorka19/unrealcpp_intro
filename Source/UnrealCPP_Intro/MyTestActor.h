// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "MyTestActor.generated.h"

UCLASS()
class UNREALCPP_INTRO_API AMyTestActor : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AMyTestActor();
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "TestValues") int32 Value1;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Transient, Category = "TestValues") int32 Value2;
	// Transient - will not be loaded or saved on disk
	
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
